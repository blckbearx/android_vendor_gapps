PRODUCT_SOONG_NAMESPACES += \
    vendor/gapps/basic

PRODUCT_COPY_FILES += \
    vendor/gapps/basic/product/etc/sysconfig/nexus.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/nexus.xml \
    vendor/gapps/basic/product/etc/sysconfig/wellbeing.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/wellbeing.xml \
    vendor/gapps/basic/product/lib64/libjni_latinimegoogle.so:$(TARGET_COPY_OUT_PRODUCT)/lib64/libjni_latinimegoogle.so \
    vendor/gapps/basic/product/lib64/libsketchology_native.so:$(TARGET_COPY_OUT_PRODUCT)/lib64/libsketchology_native.so \
    vendor/gapps/basic/product/lib/libjni_latinimegoogle.so:$(TARGET_COPY_OUT_PRODUCT)/lib/libjni_latinimegoogle.so

PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    GoogleBackupTransport \
    GoogleCalendarSyncAdapter \
    GooglePackageInstaller \
    GoogleRestore \
    LatinIMEGooglePrebuilt \
    MarkupGoogle \
    SetupWizard \
    SoundPickerGooglePrebuilt \
    Velvet \
    WellbeingPrebuilt

# Properties
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.ime.bs_theme=true \
    ro.com.google.ime.theme_id=5 \
    ro.opa.eligible_device=true \
    ro.setupwizard.rotation_locked=true \
    setupwizard.theme=glif_v3_light

ifeq ($(PRODUCT_GMS_CLIENTID_BASE),)
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.clientidbase=android-google
else
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.clientidbase=$(PRODUCT_GMS_CLIENTID_BASE)
endif

$(call inherit-product, vendor/gapps/core/config.mk)
